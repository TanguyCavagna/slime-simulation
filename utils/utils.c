#include "utils.h"

/**
 * @brief Returns a random position inside a unit circle
 *
 * @return vector_t
 */
vector_t inside_unit_circle() {
    float r = sqrt(rand() / (double)RAND_MAX);
    float alpha = (rand() / (double)RAND_MAX) * M_PI * 2.0;
    vector_t res = {0.0, 0.0};

    // map [-1; 1] to [0; 1]
    res.x = ((r * cos(alpha) + 1.0) / 2.0);
    res.y = ((r * sin(alpha) + 1.0) / 2.0);

    return res;
}
