#ifndef UTILS_H
#define UTILS_H

#include <math.h>
#include <stdlib.h>

#define M_PI 3.14159265358979323846 /* pi */

typedef struct _vector {
    float x, y;
} vector_t;

vector_t inside_unit_circle();

#endif // UTILS_H
