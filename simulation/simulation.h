#include "../agent/agent.h"
#include "../trail/trail.h"

#define AGENT_COUNT 10000
#define DELTA_TIME 1
#define SIM_SPAWN_SCALE 3.0 / 4.0

typedef struct _simulation {
    agent_t **agents;
    float *trail_map;
    int width, height;
} simulation_t;

void simulation_render(simulation_t *sim);
void simulation_update(simulation_t *sim);
void simulation_dispose(simulation_t *sim);
void simulation_setup(simulation_t *sim, int w, int h);
