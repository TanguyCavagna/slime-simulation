#include "simulation.h"

enum SpawnMode { PointMode, RandomMode, InwardCircleMode, RandomCircleMode };

/**
 * @brief Render the simulation
 *
 * @param sim
 */
void simulation_render(simulation_t *sim) {
    for (int i = 0; i < AGENT_COUNT; i++)
        draw_agent(sim->agents[i]);

    draw_trail(sim->trail_map, sim->width, sim->height);
}

/**
 * @brief Update the simulation
 *
 * @param sim
 */
void simulation_update(simulation_t *sim) {
    for (int i = 0; i < AGENT_COUNT; i++) {
        update_agent(sim->agents[i], sim->trail_map, DELTA_TIME, sim->width,
                     sim->height);

        int x = sim->agents[i]->pos->x;
        int y = sim->agents[i]->pos->y;

        sim->trail_map[sim->width * y + x] = 1;
    }

    diffuse_trail(sim->trail_map, sim->width, sim->height, DELTA_TIME);
}

/**
 * @brief Dispose the simulation
 *
 * @param sim
 */
void simulation_dispose(simulation_t *sim) {
    for (int i = 0; i < AGENT_COUNT; i++)
        free(sim->agents[i]);

    free(sim->trail_map);
    free(sim);
}

/**
 * @brief Setup the simulation
 *
 * @param sim
 * @param w Screen width
 * @param h Screen height
 */
void simulation_setup(simulation_t *sim, int w, int h) {
    enum SpawnMode spawn_mode = PointMode;

    sim->agents = (agent_t **)malloc(sizeof(agent_t) * AGENT_COUNT);
    sim->trail_map = (float *)malloc(sizeof(float) * w * h);
    sim->width = w;
    sim->height = h;

    // setup trail map
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++)
            sim->trail_map[w * j + i] = 0.0;
    }

    for (int i = 0; i < AGENT_COUNT; i++) {
        sim->agents[i] = create_dummy_agent();
        sim->agents[i]->species_mask = rand() % SPECIES_COUNT;

        float cx = w / 2;
        float cy = h / 2;
        vector_t unit_circle;
        float wh_ratio;
        float random_angle = rand() / (double)RAND_MAX * M_PI * 2.0;
        position_t start_pos = {0.0, 0.0};
        float angle = 0;

        switch (spawn_mode) {
        case PointMode:
            start_pos.x = cx;
            start_pos.y = cy;
            angle = random_angle;
            break;

        case RandomMode:
            start_pos.x = rand() % w;
            start_pos.y = rand() % h;
            angle = random_angle;
            break;

        case RandomCircleMode:
        case InwardCircleMode:
            unit_circle = inside_unit_circle();

            wh_ratio = (float)h / (float)w;
            start_pos.x = cx + unit_circle.x * w * SIM_SPAWN_SCALE * wh_ratio;
            start_pos.y = cy + unit_circle.y * h * SIM_SPAWN_SCALE;

            // centering
            start_pos.x -= SIM_SPAWN_SCALE * w / 2.0 * wh_ratio;
            start_pos.y -= SIM_SPAWN_SCALE * h / 2.0;

            if (spawn_mode == InwardCircleMode)
                angle = atan2f((cx - start_pos.x), (cy - start_pos.y));
            else if (spawn_mode == RandomCircleMode)
                angle = random_angle;
            break;
        }

        *(sim->agents[i]->pos) = start_pos;
        sim->agents[i]->angle = angle;
    }
}
