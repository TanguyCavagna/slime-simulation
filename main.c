/**
 * @file main.c
 * @author Tanguy Cavagna (tanguy.cavagna@etu.hesge.ch)
 * @brief Main file for the electric field exercice
 * @version 0.1
 * @date 2022-04-13
 *
 * @copyright Copyright (c) 2021
 *
 */
#include "simulation/simulation.h"
#include <GL/freeglut.h>
#include <GL/freeglut_std.h>
#include <GL/glut.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// https://stackoverflow.com/questions/3417837/what-is-the-best-way-to-suppress-a-unused-variable-x-warning
#ifdef UNUSED
#elif defined(__GNUC__)
#define UNUSED(x) UNUSED_##x __attribute__((unused))
#elif defined(__LCLINT__)
#define UNUSED(x) /*@unused@*/ x
#else
#define UNUSED(x) x
#endif

#define ONE_SECOND_IN_MILLISECONDS 1000
#define REFRESH_RATE 60

#define WIDTH 1280
#define HEIGHT 720

simulation_t *sim;

/**
 * @brief Update function
 */
void update() { simulation_update(sim); };

/**
 * @brief Update timer
 */
void update_timer() {
    update();
    glutTimerFunc(ONE_SECOND_IN_MILLISECONDS / REFRESH_RATE, update_timer, 0);
}

/**
 * @brief Draw timer
 */
void draw_timer() {
    glutPostRedisplay();
    glutTimerFunc(ONE_SECOND_IN_MILLISECONDS / REFRESH_RATE, draw_timer, 0);
}

/**
 * @brief Display function of Glut
 */
void display() {
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    simulation_render(sim);

    glutSwapBuffers();
}

/**
 * @brief Keyboard handler
 *
 * @param key
 * @param UNUSED
 * @param UNUSED
 */
void keyboard(unsigned char key, int UNUSED(x), int UNUSED(y)) {
    switch (key) {
    case 'q':
        glutLeaveMainLoop();
        break;

    default:
        break;
    }
}

/**
 * @brief Main function
 *
 * @param argc
 * @param argv
 * @return int
 */
int main(int argc, char **argv) {
    srand(time(NULL));

    sim = (simulation_t *)malloc(sizeof(simulation_t));
    simulation_setup(sim, WIDTH, HEIGHT);

    // Window initialization
    glutInit(&argc, argv);
    glutSetOption(GLUT_MULTISAMPLE, 16);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH |
                        GLUT_MULTISAMPLE);
    glutInitWindowSize(WIDTH, HEIGHT);
    int window = glutCreateWindow("Slime simulation");

    glClearColor(0, 0, 0, 1);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, WIDTH, HEIGHT, 0);

    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);

    glutTimerFunc(ONE_SECOND_IN_MILLISECONDS / REFRESH_RATE, update_timer, 0);
    glutTimerFunc(ONE_SECOND_IN_MILLISECONDS / REFRESH_RATE, draw_timer, 0);

    glutMainLoop();

    // Object destroy
    glutDestroyWindow(window);
    simulation_dispose(sim);
    return EXIT_SUCCESS;
}
