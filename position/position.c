#include "position.h"

position_t *create_null_pos() {
    position_t *res = (position_t *)malloc(sizeof(position_t *));

    res->x = 0.0;
    res->y = 0.0;

    return res;
}