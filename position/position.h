#include <stdlib.h>

typedef struct _position {
    float x, y;
} position_t;

position_t *create_null_pos();