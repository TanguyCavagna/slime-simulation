#include "agent.h"

/**
 * @brief Sense values in the given angle
 *
 * @param a Agent to use
 * @param sensor_angle_offset Sensor angle
 * @return float Value sensed
 */
float sense(const agent_t *a, float *trail_map, float sensor_angle_offset,
            int w, int h) {
    float sensor_angle = a->angle + sensor_angle_offset;
    vector_t sensor_dir = {cos(sensor_angle), sin(sensor_angle)};
    vector_t sensor_center = {a->pos->x + sensor_dir.x * SENSOR_DST,
                              a->pos->y + sensor_dir.y * SENSOR_DST};
    float sum = 0;

    for (int offset_x = -SENSOR_SIZE; offset_x <= SENSOR_SIZE; offset_x++) {
        for (int offset_y = -SENSOR_SIZE; offset_y <= SENSOR_SIZE; offset_y++) {
            vector_t pos = {sensor_center.x + offset_x,
                            sensor_center.y + offset_y};

            if (pos.x >= 0 && pos.x < w && pos.y >= 0 && pos.y < h) {
                sum += trail_map[w * (int)pos.y + (int)pos.x];
            }
        }
    }

    return sum;
}

/**
 * @brief Create a dummy agent
 *
 * @return agent_t*
 */
agent_t *create_dummy_agent() {
    agent_t *res = (agent_t *)malloc(sizeof(agent_t));

    res->angle = 0.0;
    res->pos = create_null_pos();

    return res;
}

/**
 * @brief Update the agent position
 *
 * @param a Agent to update
 * @param dt Delta time
 */
void update_agent(agent_t *a, float *trail_map, double dt, int w, int h) {
    // steering based on sensory data
    float sensor_angle = SENSOR_ANGLE * (M_PI / 180.0);
    float weight_forward = sense(a, trail_map, 0, w, h);
    float weight_left = sense(a, trail_map, sensor_angle, w, h);
    float weight_right = sense(a, trail_map, -sensor_angle, w, h);

    float random_sterring_strength = rand() / (double)RAND_MAX;
    float turn_speed = TURN_SPEED * M_PI * 2.0;

    // continue in same direction
    if (weight_forward > weight_left && weight_forward > weight_right) {
        a->angle += 0;
    }
    // turn randomly
    else if (weight_forward < weight_left && weight_forward < weight_right) {
        a->angle += (random_sterring_strength - 0.5) * 2 * turn_speed * dt;
    }
    // turn right
    else if (weight_right > weight_left) {
        a->angle -= random_sterring_strength * turn_speed * dt;
    }
    // turn left
    else if (weight_left > weight_right) {
        a->angle += random_sterring_strength * turn_speed * dt;
    }

    vector_t direction = {cos(a->angle), sin(a->angle)};
    position_t new_pos = {a->pos->x + direction.x * dt * MOVE_SPEED,
                          a->pos->y + direction.y * dt * MOVE_SPEED};

    if (new_pos.x < 0 || new_pos.x >= w || new_pos.y < 0 || new_pos.y >= h) {
        new_pos.x = fmax(0, fmin(new_pos.x, w - 1));
        new_pos.y = fmax(0, fmin(new_pos.y, h - 1));
        a->angle = (rand() / (double)RAND_MAX) * M_PI * 2.0;
    }

    a->pos->x = new_pos.x;
    a->pos->y = new_pos.y;
}

/**
 * @brief Draw the agent
 *
 * @param a Agent to draw
 */
void draw_agent(const agent_t *a) {
    position_t p = {a->pos->x, a->pos->y};

    glColor3f(0.12, 0.84, 0.87);
    glPointSize(CELL_SIZE);
    glBegin(GL_POINTS);
    glVertex2f(p.x, p.y);
    glEnd();
}
