#include "../position/position.h"
#include "../utils/utils.h"
#include <GL/freeglut.h>
#include <GL/freeglut_std.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define MOVE_SPEED 1
#define CELL_SIZE 2
#define SENSOR_DST 2
#define SENSOR_SIZE 3
#define SENSOR_ANGLE 45
#define TURN_SPEED 0.25

// species colors
#define SPECIES_COUNT 2
#define SPECIES_COLOR_SHIFT 4

#define SPECIES_A_R 0.12
#define SPECIES_A_G 0.84
#define SPECIES_A_B 0.87

#define SPECIES_B_R 0.92
#define SPECIES_B_G 0.51
#define SPECIES_B_B 0.13

typedef struct _agent {
    position_t *pos;
    float angle;
    int species_mask;
} agent_t;

agent_t *create_dummy_agent();
void update_agent(agent_t *a, float *trail_map, double dt, int w, int h);
void draw_agent(const agent_t *a);
