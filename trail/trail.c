#include "trail.h"

/**
 * @brief Linear interpolation of a value between two points
 *
 * @param a Point a
 * @param b Point b
 * @param t Value to interpolate
 * @return float
 */
float lerp(float a, float b, float t) { return a * (1.0 - t) + (b * t); }

/**
 * @brief Draw the trail
 *
 * @param trail_map
 * @param w
 * @param h
 */
void draw_trail(float *trail_map, int w, int h) {
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            float intensity = trail_map[w * j + i];

            glColor3f(intensity * 0.12, intensity * 0.84, intensity * 0.87);
            glBegin(GL_POINTS);
            glVertex2f(i, j);
            glEnd();
        }
    }
}

/**
 * @brief Diffuse the trail
 *
 * @param trail_map
 * @param w Screen width
 * @param h Screen height
 * @param dt Delta time
 */
void diffuse_trail(float *trail_map, int w, int h, double dt) {
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            float intensity = trail_map[w * j + i];

            // blur on a 3x3 grid
            float sum = 0;
            for (int offset_x = -1; offset_x <= 1; offset_x++) {
                for (int offset_y = -1; offset_y <= 1; offset_y++) {
                    int sample_x = i + offset_x;
                    int sample_y = j + offset_y;

                    if (sample_x >= 0 && sample_x < w && sample_y >= 0 &&
                        sample_y < h) {
                        sum += trail_map[w * sample_y + sample_x];
                    }
                }
            }
            float blur = sum / 9;
            float diffused_value = lerp(intensity, blur, DIFFUSION_SPEED * dt);

            trail_map[w * j + i] = fmax(0, diffused_value - DECAY_RATE * dt);
        }
    }
}
