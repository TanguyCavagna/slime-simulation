#include "../utils/utils.h"
#include <GL/freeglut.h>
#include <GL/freeglut_std.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define DECAY_RATE 0.005
#define DIFFUSION_SPEED 0.5

void draw_trail(float *trail_map, int w, int h);
void diffuse_trail(float *trail_map, int w, int h, double dt);
